module.exports.problem5 = function(inventory){
    var carYears = [];

    for(let i=0; i<inventory.length; i++){
        carYears[i] = inventory[i].car_year;
    }
    var count = 0;
    for(let i=0; i<carYears.length; i++){
        if(carYears[i] < 2000){
            count++;
        }
    }
    console.log(count);
}